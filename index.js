// -------------------------------------------------------Class & global variable -------------------------------------------------
var isDrawing = false;
var isFill = false;
var width = 0;
var height = 0;
var status = "unknown";
var drawing = [];
var color = 'black';

var lineW = 10;
var current = 0;

function Point(x, y){
    this.x = x;
    this.y = y;
}

function line(){
    this.status = "unknown";
    this.array = [];
    this.width = 0;
    this.color = 'rbga(0,0,0,0)';
}

function rectangle(x, y){
    this.status = "unknown";
    this.width = 0;
    this.height = 0;
    this.anchorX = x;
    this.anchorY = y;
    this.lineWidth = 0;
    this.color = 'rbga(0,0,0,0)';
}

function circle(x, y, x2, y2){
    this.status = "unknown";
    this.width = 0;
    this.height = 0;
    this.anchor = new Point(x, y);
    this.ending = new Point(x2,y2)
    this.lineWidth = 0;
    this.color = 'rbga(0,0,0,0)';
}

function triangle(x, y, x2, y2){
    this.status = "unknown";
    this.width = 0;
    this.height = 0;
    this.anchor = new Point(x, y);
    this.ending = new Point(x2,y2)
    this.lineWidth = 0;
    this.color = 'rbga(0,0,0,0)';
}

function text(){
    this.status = "text";
    this.content = "";
    this.font = "";
    this.fontSize = 0;
    this.locationX = 0;
    this.locationY = 0;
}


var startingPoint = new Point(0, 0);
var recordLine = new line();

//--------------------------------------------------------------- function-----------------------------------------------------------
function drawLine(p1, p2, strokeColor, strokeWidth){

    var canvas = document.getElementById("canvas");
    var canvasContext = canvas.getContext("2d");

    canvasContext.globalCompositeOperation="source-over";
    canvasContext.moveTo(p1.x, p1.y);
    canvasContext.lineTo(p2.x, p2.y);
    canvasContext.lineCap = "round";
    canvasContext.lineJoin = "round";
    canvasContext.lineWidth = strokeWidth;
    canvasContext.strokeStyle = strokeColor;
    console.log("stroke" + " " + strokeColor);
    canvasContext.stroke();

}

function eraseLine(p1, p2, strokeWidth){
    var canvas = document.getElementById("canvas");
    var canvasContext = canvas.getContext("2d");

    canvasContext.globalCompositeOperation="destination-out";
    console.log("destination out");
    canvasContext.moveTo(p1.x, p1.y);
    canvasContext.lineTo(p2.x, p2.y);
    canvasContext.lineCap = "round";
    canvasContext.lineJoin = "round";
    canvasContext.lineWidth = strokeWidth;
    canvasContext.stroke();
}

function drawRect(p1,p2, strokeColor, strokeWidth){

    var canvas = document.getElementById("canvas");
    var canvasContext = canvas.getContext("2d");

    canvasContext.beginPath();
    
    canvasContext.rect(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y);

    if(isFill){
        canvasContext.fillStyle = strokeColor;
        canvasContext.fill();
    }
    
    canvasContext.lineWidth = strokeWidth;
    canvasContext.globalCompositeOperation="source-over";
    canvasContext.strokeStyle = strokeColor;
    canvasContext.lineCap = "round";
    canvasContext.lineJoin = "round";
    canvasContext.stroke();

}

function drawTriangle(p1,p2, strokeColor, strokeWidth, status){

    var canvas = document.getElementById("canvas");
    var canvasContext = canvas.getContext("2d");

    canvasContext.beginPath();

    //canvasContext.rect(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y);
    canvasContext.moveTo((p1.x + p2.x) /2, p1.y);
    canvasContext.lineTo(p2.x, p2.y);
    canvasContext.lineTo(p1.x, p2.y);
    canvasContext.lineTo((p1.x + p2.x) /2, p1.y);
    canvasContext.lineCap = "round";
    canvasContext.lineJoin = "round";
    canvasContext.lineWidth = strokeWidth;
    canvasContext.globalCompositeOperation="source-over";
    canvasContext.strokeStyle = strokeColor
    
    if(isFill){
        canvasContext.fillStyle = strokeColor;
        canvasContext.fill();
    }
    canvasContext.stroke();

}

function drawCircle(p1,p2, strokeColor, strokeWidth, status){
    var canvas = document.getElementById("canvas");
    var canvasContext = canvas.getContext("2d");

    canvasContext.beginPath();

    canvasContext.arc((p2.x + p1.x)/2, (p2.y + p1.y)/2 ,Math.abs((p2.x - p1.x)/2),0*Math.PI,2*Math.PI);
    canvasContext.lineWidth = strokeWidth;
    canvasContext.globalCompositeOperation="source-over";
    canvasContext.strokeStyle = strokeColor;
    if(isFill){
        canvasContext.fillStyle = strokeColor;
        canvasContext.fill();
    }
    canvasContext.stroke();
}

function redraw(){
    var i = 0;
    var canvas = document.getElementById("canvas");
    var canvasContext = canvas.getContext("2d");
    var finish = (isDrawing) ? current -2  : current - 1;

    if(finish>=0 && finish < drawing.length){
        canvasContext.putImageData(drawing[finish],0,0);
    }
        
}

//------------------------------------------------------- Mouse Event--------------------------------------------------

function startPen(event) {

    if(status == "pencil" || status == "eraser" || status == "rectangle" || status == "circle" || status == "triangle"){

        var canvas = document.getElementById("canvas");
        var canvasContext = canvas.getContext("2d");

        var nextPoint = new Point(event.offsetX, event.offsetY);
        
        current ++;
        
        canvasContext.beginPath();

        isDrawing = true;
        startingPoint = nextPoint;

    }
}

function dragPen(event){

    var canvas = document.getElementById("canvas");
    var canvasContext = canvas.getContext("2d");
    
    var nextPoint = new Point(event.offsetX, event.offsetY);
    changeCursor();

    if(isDrawing && status == "pencil"){

        drawLine(startingPoint, nextPoint, color, lineW);
       
        recordLine.status = "pencil";
        recordLine.width = lineW;
        recordLine.color = color;

        recordLine.array.push([startingPoint, nextPoint]);
        
        startingPoint = nextPoint;
    }

    else if(isDrawing && status =="eraser"){

        
        eraseLine(startingPoint, nextPoint, lineW);

        recordLine.status = "eraser";
        recordLine.width = lineW;

        recordLine.array.push([startingPoint, nextPoint]);
        
        startingPoint = nextPoint;
    }

    else if(isDrawing && status == "rectangle"){

        // console.log("clear");
        canvasContext.clearRect(0, 0, 900 ,600);
        redraw();
        drawRect(startingPoint, nextPoint, color, lineW);
    }

    else if(isDrawing && status == "circle"){

        // console.log("clear");
        canvasContext.clearRect(0, 0, 900 ,600);
        redraw();
        drawCircle(startingPoint, nextPoint, color, lineW);
    }

    else if(isDrawing && status == "triangle"){

        // console.log("clear");
        canvasContext.clearRect(0, 0, 900 ,600);
        redraw();
        drawTriangle(startingPoint, nextPoint, color, lineW);
    }

    
}

function stopPen(event){

    var canvas = document.getElementById("canvas");
    var canvasContext = canvas.getContext("2d");

    var nextPoint = new Point(event.offsetX, event.offsetY);
    console.log(current);

    if(isDrawing && status == "pencil"){

        drawLine(startingPoint, nextPoint, color, lineW);

        recordLine.array.push([startingPoint, nextPoint]);
        
        startingPoint = nextPoint;

        while(drawing.length != current - 1)
        {
            drawing.pop();
        }

    }

    else if(isDrawing && status =="eraser"){

        eraseLine(startingPoint, nextPoint, lineW);
        
        while(drawing.length != current - 1)
        {
            drawing.pop();
        }

    }

    else if(isDrawing && status == "rectangle"){

        canvasContext.clearRect(0, 0, 900 ,600);
        redraw();
        drawRect(startingPoint, nextPoint, color, lineW);

        while(drawing.length != current - 1)
        {
            drawing.pop();
        }
    }

    else if(isDrawing && status == "circle"){

        canvasContext.clearRect(0, 0, 900 ,600);
        redraw();
        drawCircle(startingPoint, nextPoint, color, lineW);

        while(drawing.length != current - 1)
        {
            drawing.pop();
        }

    }

    else if(isDrawing && status == "triangle"){

        canvasContext.clearRect(0, 0, 900 ,600);
        redraw();
        drawTriangle(startingPoint, nextPoint, color, lineW);

        while(drawing.length != current - 1)
        {
            drawing.pop();
        }
    }
    
    if(isDrawing){
        var newImage = canvasContext.getImageData(0, 0, 900, 600);
        drawing.push(newImage);
    }
    
    console.log(status + current + " " + drawing.length);
    isDrawing = false;
}

//-------------------------------------------- Button Event ----------------------------------------------------------

function changeIcon(value){

    console.log(value);

    var tb = document.getElementById("btnClear");
    tb.src = "image/refresh1.png";
    tb = document.getElementById("btnUndo");
    tb.src = "image/undo1.png";
    tb = document.getElementById("btnRedo");
    tb.src = "image/redo1.png";
    tb = document.getElementById("btnPencil");
    tb.src = "image/pencil1.png";
    b = document.getElementById("btnEraser");
    b.src = "image/eraser1.png";
    b = document.getElementById("btnText");
    b.src = "image/text1.png";
    b = document.getElementById("btnCircle");
    b.src = "image/circle1.png";
    b = document.getElementById("btnRectangle");
    b.src = "image/rectangle1.png";
    b = document.getElementById("btnTriangle");
    b.src = "image/triangle1.png";
    b = document.getElementById("btnDownload");
    b.src = "image/download1.png";



    if(value == "pencil"){
        var b = document.getElementById("btnPencil");
        b.src = "image/pencil2.png";
    }
    else if(value == "eraser"){
        var b = document.getElementById("btnEraser");
        b.src = "image/eraser2.png";
    }
    else if(value == "text"){
        var b = document.getElementById("btnText");
        b.src = "image/text2.png";
    }
    else if(value == "circle"){
        var b = document.getElementById("btnCircle");
        b.src = "image/circle2.png";
    }
    else if(value == "rectangle"){
        var b = document.getElementById("btnRectangle");
        b.src = "image/rectangle2.png";
    }
    else if(value == "triangle"){
        var b = document.getElementById("btnTriangle");
        b.src = "image/triangle2.png";
    }
    else if(value == "uploadImage"){
        var b = document.getElementById("btnUpload");
        b.src = "image/upload2.png";
    }
    else if(value == "downloadImage"){
        var b = document.getElementById("btnDownload");
        b.src = "image/download2.png";
    }
}



function btnClicked(value){
    btn = document.getElementById("btn");
    var canvas = document.getElementById("canvas");
    var canvasContext = canvas.getContext("2d");
    var b = document.getElementById("btnClear");

    b.addEventListener("mousedown", function(){
        var tb = document.getElementById("btnClear");
        tb.src = "image/refresh2.png";
    });

    b = document.getElementById("btnRedo");

    b.addEventListener("mousedown", function(){
        var tb = document.getElementById("btnRedo");
        tb.src = "image/redo2.png";
    });

    b = document.getElementById("btnUndo");

    b.addEventListener("mousedown", function(){
        var tb = document.getElementById("btnUndo");
        tb.src = "image/undo2.png";
    });

    b = document.getElementById("btnDownload");

    b.addEventListener("mousedown", function(event){
        var tb = document.getElementById("btnDownload");
        tb.src = "image/download2.png";
    });
    

    if(value == "clear"){
        changeIcon(value);
        canvasContext.clearRect(0, 0, 900 ,600);
        drawing = [];
        current = 0;
    }

    else if(value == "undo"){
        changeIcon(value);
        current = (current == 0) ? current : current-1;
        console.log(current);
        canvasContext.clearRect(0, 0, 900 ,600);
        redraw();
    }

    else if(value == "redo"){
        changeIcon(value);
        current = (current >= drawing.length ) ? current : current + 1;
        canvasContext.clearRect(0, 0, 900 ,600);
        redraw();
    }

    else if(value == "downloadImage"){
        changeIcon(value);
    }


    else{
        status = value;
    }

    changeIcon(status);
    checkTextBox(status);
}

//---------------------------------------------- Fill -------------------------------------------

function checkFill(){
    if(isFill){
        isFill = false;
        var b = document.getElementById("btnFill");
        b.src = "image/fill1.png";
    }

    else {
        isFill = true;
        var b = document.getElementById("btnFill");
        b.src = "image/fill2.png";
    }
}

//-----------------------------------------------Download----------------------------------------

download_img = function(el) {
    var image = canvas.toDataURL("image/jpg");
    el.href = image;
};
  

//---------------------------------------------- Text -----------------------------------------------
var isTyping = false;
var textPosition = new Point(0,0);

function clickedCanvas(event){
    if(status == "text"){
        inputText(event);
    }
}

function inputText(event){

    var canvas = document.getElementById("canvas");
    var canvasContext = canvas.getContext("2d");

    if(status == "text" && !isTyping){

        var textBox = document.createElement("INPUT");
        textBox.setAttribute("type", "text");
        textBox.setAttribute("style", "z-index:2");
        textBox.id = "textBox";

        var division = document.createElement("div");
        division.appendChild(textBox);
        division.id = "textDiv";
        document.body.appendChild(division);

        division.style.position = "absolute";
        division.style.left = event.x;
        division.style.top = event.y;

        textPosition.x = event.offsetX;
        textPosition.y = event.offsetY;

        isTyping = true;
        
    }

    else if (status =="text") {

        var textBox = document.getElementById("textBox");

        canvasContext.font = document.getElementById("font_size").value + "px " + document.getElementById("font").value;
        canvasContext.fillStyle = color;
        canvasContext.globalCompositeOperation="source-over"
        canvasContext.fillText(textBox.value , textPosition.x, textPosition.y);

        if(textBox.value != ""){

            current ++;

            while(drawing.length != current - 1)
            {
                drawing.pop();
            }

            var newImage = canvasContext.getImageData(0, 0, 900, 600);
            drawing.push(newImage);
        }
        
        document.body.removeChild(document.getElementById("textDiv"));
        isTyping = false;
    }

    
    
}

function checkTextBox(status){
    if(isTyping && status != "text"){
        
        var textBox = document.getElementById("textBox");
        var canvas = document.getElementById("canvas");
        var canvasContext = canvas.getContext("2d");

        if(textBox.value != ""){


            canvasContext.font = document.getElementById("font_size").value + "px " + document.getElementById("font").value;
            canvasContext.fillStyle = color;
            canvasContext.globalCompositeOperation="source-over"
            canvasContext.fillText(textBox.value , textPosition.x, textPosition.y);

            current ++;

            while(drawing.length != current - 1)
            {
                drawing.pop();
            }

            var newImage = canvasContext.getImageData(0, 0, 900, 600);
            drawing.push(newImage);
        }


        document.body.removeChild(document.getElementById("textDiv"));
        isTyping = false;
    }
}
//---------------------------------------------- Slider Event------------------------------------------

function changeLineWidth(value){
    lineW = value / 5;
    console.log(lineW);
}

//--------------------------------------------- Clolor Picker ----------------------------------------

var isChoosing = false;
var rgbacolor = 'rgba(255,0,0,1)';

function initColor(){

    colorChoose();

    var colorBlock = document.getElementById('color-block');
    var blockContext = colorBlock.getContext('2d');

    var colorStrip = document.getElementById('color-strip');
    var stripContext = colorStrip.getContext('2d');

    blockContext.rect(0, 0, colorBlock.width, colorBlock.height);
    fillGradient();

    stripContext.rect(0, 0, colorStrip.width, colorStrip.height);
    var gradient = stripContext.createLinearGradient(0, 0, 0, colorBlock.height);
    gradient.addColorStop(0, 'rgba(255, 0, 0, 1)');
    gradient.addColorStop(0.17, 'rgba(255, 255, 0, 1)');
    gradient.addColorStop(0.34, 'rgba(0, 255, 0, 1)');
    gradient.addColorStop(0.51, 'rgba(0, 255, 255, 1)');
    gradient.addColorStop(0.68, 'rgba(0, 0, 255, 1)');
    gradient.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
    gradient.addColorStop(1, 'rgba(255, 0, 0, 1)');
    stripContext.fillStyle = gradient;
    stripContext.fill();
}


function colorType(event){

    var colorStrip = document.getElementById('color-strip');
    var stripContext = colorStrip.getContext('2d');

    var imageData = stripContext.getImageData(event.offsetX, event.offsetY, 1, 1).data;
    rgbacolor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    fillGradient();
}

function fillGradient() {

    var colorBlock = document.getElementById('color-block');
    var blockContext = colorBlock.getContext('2d');

    var colorStrip = document.getElementById('color-strip');
    var stripContext = colorStrip.getContext('2d');

    blockContext.fillStyle = rgbacolor;
    blockContext.fillRect(0, 0, colorBlock.width, colorBlock.height);

    var grdWhite = stripContext.createLinearGradient(0, 0, colorBlock.width, 0);
    grdWhite.addColorStop(0, 'rgba(255,255,255,1)');
    grdWhite.addColorStop(1, 'rgba(255,255,255,0)');
    blockContext.fillStyle = grdWhite;
    blockContext.fillRect(0, 0, colorBlock.width, colorBlock.height);

    var grdBlack = stripContext.createLinearGradient(0, 0, 0, colorBlock.height);
    grdBlack.addColorStop(0, 'rgba(0,0,0,0)');
    grdBlack.addColorStop(1, 'rgba(0,0,0,1)');
    blockContext.fillStyle = grdBlack;
    blockContext.fillRect(0, 0, colorBlock.width, colorBlock.height);
}

function startSelection(event) {
    isChoosing = true;
    changeColor(event);
}

function selectingColor(event) {
    if (isChoosing) {
        changeColor(event);
    }
}

function confirmColor(event) {
    isChoosing = false;
}

function changeColor(event) {

    var colorBlock = document.getElementById('color-block');
    var blockContext = colorBlock.getContext('2d');

    var imageData = blockContext.getImageData(event.offsetX, event.offsetY, 1, 1).data;
    rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    color = rgbaColor;
    colorChoose();
}


function colorChoose(){
    currentColor = document.getElementById("currentColor");
    currentColor.style.background = color;
}

//----------------------------------------------------- Change Cursor ----------------------------------------------

function changeCursor(){
    
    var canvasDiv = document.getElementById("canvas");

    if(status == "pencil"){
        canvasDiv.style.cursor = "url(cur/pencilCursor.cur),default";
    }

    else if(status == "eraser"){
        canvasDiv.style.cursor = "url(cur/eraser.cur),default";
    }

    else if(status == "text"){
        canvasDiv.style.cursor = "text";
    }

    else if(status == "rectangle"){
        canvasDiv.style.cursor = "url(cur/silvereyes_alt.cur),default";
    }

    else if(status == "circle"){
        canvasDiv.style.cursor = "url(cur/circle.cur),default";
    }

    else if(status == "triangle"){
        canvasDiv.style.cursor = "url(cur/triangle.cur),default";
    }

    else{
        canvasDiv.style.cursor = "initial";
    }
}

//------------------------------------------------ upload image -----------------------------------------------

function uploadImage(event){

        document.getElementById("selectFile").onchange = function(e){
            var canvas = document.getElementById("canvas");
            var canvasContext = canvas.getContext("2d");
    
                var input = e.target;
                var output = new Image();
                console.log("outside");
            
                var reader = new FileReader();
                output.onload = function(){
                    var dataURL = reader.result;
                    current++;

                    while(drawing.length != current - 1)
                    {
                        drawing.pop();
                    }

                   
                    canvasContext.drawImage(output,0,0,canvas.width,canvas.height);
                    var newImage = canvasContext.getImageData(0, 0, 900, 600);
                    drawing.push(newImage);
                };
        
                output.src = window.URL.createObjectURL(this.files[0]);
        } 
}


