# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | Y         |


---

### How to use 

<p><img src="screenshot/color.png"></p>

   We can choose color for lines, text, and shapes with the color palete above. 
   The thin block is used to choose the type of color while the main block allows us to choose its intensity.

<p><img src="screenshot/arrow.png"></p> 

    These are the clear, undo and redo button.

<p><img src="screenshot/tool.png"></p> 

    We can draw with the pencil and erase with the eraser. It can be activated by clicking and dragging on the canvas.
    To type in a text, we can first click on the canvas and a text box will appear, tap on the canvas again if you want to cancel your action.
    After typing in, click the canvas again to input the text, the location of the text will follow the location the user first clicked.

<p><img src="screenshot/size.png"></p> 
    The slider above can control the size of brish and eraser, it can also adjust the width of the outline of the shapes.
    users can choose fonts and font size from the input above. Note that the slider does not cover font size and it is the number input that controls it.

<p><img src="screenshot/shape.png"></p> 

    The shape brush allows users to create outlines of the chosen brush. Hold to resize the shape chosen.

<p><img src="screenshot/image.png"></p> 

    uploading an image will allow user to put the image into the canvas, it will automatically follow the size of the canvas.
    download allows users to download images drawn on the canvas.

    Besides cursor will change according to the tools used.

### Function description

<p><img src="screenshot/fill.png"></p> 
<p><img src="screenshot/fills.png"></p> 
    After clicking the fill button, the fill function will be activated (the button will show green if activated). 
    While fill is activated, shapes drawn will be filled instead of having only the outline.

### Gitlab page link

   https://108000166.gitlab.io/AS_01_WebCanvas

### Others (Optional)


<style>
table th{
    width: 100%;
}
</style>